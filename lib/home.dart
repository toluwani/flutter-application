import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
    int bottomNavIndex= 0;

  @override
  Widget build(BuildContext context) {
        return Scaffold(
          bottomNavigationBar: new BottomNavigationBar( 
            type: BottomNavigationBarType.fixed,
            fixedColor: Color(0xFF18D191),
            currentIndex:bottomNavIndex,
        onTap: (int index){
setState(() {
  bottomNavIndex =index; 
});
        },
        items: [
          new BottomNavigationBarItem(
            title:new Text(""),
          icon: new Icon(Icons.home)
          ),
          new BottomNavigationBarItem(
            title:new Text(""),
          icon: new Icon(Icons.local_offer)
          ),
           new BottomNavigationBarItem(
            title:new Text(""),
          icon: new Icon(Icons.message)
          ),
           new BottomNavigationBarItem(
            title:new Text(""),
          icon: new Icon(Icons.notification_important)
          ),
        ],
      ),
      appBar: new AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        iconTheme: new IconThemeData(color: Color(0xFF18D191)),
      ),
      body: MainContent(),
    );
  }
}

class MainContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: new Container(
            child: new Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    new Text(
                      "Explore",
                      style: new TextStyle(
                        fontSize: 20.0,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
                new SizedBox(
                  height: 12.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                        child: Padding(
                      padding: const EdgeInsets.only(right: 5.0),
                      child: new Container(
                        height: 100.0,
                        decoration: new BoxDecoration(
                            borderRadius: new BorderRadius.circular(5.0),
                            color: Color(0xFFFC6A7F)),
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Icon(
                              Icons.drive_eta,
                              color: Colors.black,
                            ),
                            new Text(
                              "Cars",
                              style: new TextStyle(color: Colors.black),
                            )
                          ],
                        ),
                      ),
                    )),
                    new Expanded(
                        child: new Container(
                      height: 100.0,
                      child: Column(
                        children: <Widget>[
                          Expanded(
                              child: Padding(
                            padding:
                                const EdgeInsets.only(bottom: 2.5, right: 2.5),
                            child: new Container(
                              decoration: new BoxDecoration(
                                  color: Color(0xFF18D191),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: new Icon(Icons.local_offer,
                                        color: Colors.black),
                                  ),
                                  new Text("classified",
                                      style: new TextStyle(color: Colors.black))
                                ],
                              ),
                            ),
                          )),
                          Expanded(
                              child: Padding(
                            padding:
                                const EdgeInsets.only(top: 2.5, right: 2.5),
                            child: new Container(
                              decoration: new BoxDecoration(
                                  color: Color(0XFFFC7B4D),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: new Icon(Icons.beenhere,
                                        color: Colors.black),
                                  ),
                                  new Text("service",
                                      style: new TextStyle(color: Colors.black))
                                ],
                              ),
                            ),
                          )),
                        ],
                      ),
                    )),
                    new Expanded(
                        child: new Container(
                      height: 100.0,
                      child: Column(
                        children: <Widget>[
                          Expanded(
                              child: Padding(
                            padding:
                                const EdgeInsets.only(left: 2.5, bottom: 2.5),
                            child: new Container(
                              decoration: new BoxDecoration(
                                  color: Color(0XFF53CEDB),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: new Icon(Icons.account_balance,
                                        color: Colors.black),
                                  ),
                                  new Text("properties",
                                      style: new TextStyle(color: Colors.black))
                                ],
                              ),
                            ),
                          )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.only(left: 2.5, top: 2.5),
                            child: new Container(
                              decoration: new BoxDecoration(
                                  color: Color(0xFFFFCE56),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: new Icon(Icons.art_track,
                                        color: Colors.black),
                                  ),
                                  new Text("Jobs",
                                      style: new TextStyle(color: Colors.black))
                                ],
                              ),
                            ),
                          )),
                        ],
                      ),
                    )),
                  ],
                ),
                new SizedBox(
                  height: 15.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: new Text("popular now",
                          style: new TextStyle(fontSize: 18.0)),
                    ),
                    new Expanded(
                      child: new Text(
                        "view all ",
                        style: new TextStyle(
                            fontSize: 15.0, color: Color(0xFF18D191)),
                        textAlign: TextAlign.end,
                      ),
                    )
                  ],
                ),
                new SizedBox(
                  height: 12.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: Container(
                        height: 150.0,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 100,
                              decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(5.0),
                                  image: new DecorationImage(
                                      image: new NetworkImage(
                                          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0vEL6SPYFBAtk2f0nCTqYwtUPOw8y14HVPEGch_HTcoWJFvtvdQ'),
                                      fit: BoxFit.cover)),
                            ),
                            new Text(" blue excorsist",
                                style: new TextStyle(fontSize: 17.0),
                                textAlign: TextAlign.center),
                          ],
                        ),
                      ),
                    ),
                    new SizedBox(
                      width: 5.0,
                    ),
                    new Expanded(
                      child: Container(
                        height: 150.0,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 100,
                              decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(5.0),
                                  image: new DecorationImage(
                                      image: new NetworkImage(
                                          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQk8LYsDFU53HAbyOmdwgJrMVTL57FkWgutHdC3bQAIfeZ-3je_'),
                                      fit: BoxFit.cover)),
                            ),
                            new Text("mob pysco",
                                style: new TextStyle(fontSize: 17.0),
                                textAlign: TextAlign.center),
                          ],
                        ),
                      ),
                    ),
                    new SizedBox(
                      width: 5.0,
                    ),
                    new Expanded(
                      child: Container(
                        height: 150.0,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 100,
                              decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(5.0),
                                  image: new DecorationImage(
                                      image: new NetworkImage(
                                          'https://data.whicdn.com/images/296649378/large.jpg'),
                                      fit: BoxFit.cover)),
                            ),
                            new Text("Death note",
                                style: new TextStyle(fontSize: 17.0),
                                textAlign: TextAlign.center),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              
             
              new SizedBox(
                  height: 15.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: new Text("popular now",
                          style: new TextStyle(fontSize: 18.0)),
                    ),
                    new Expanded(
                      child: new Text(
                        "view all ",
                        style: new TextStyle(
                            fontSize: 15.0, color: Color(0xFF18D191)),
                        textAlign: TextAlign.end,
                      ),
                    )
                  ],
                ),
                new SizedBox(
                  height: 12.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: Container(
                        height: 150.0,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 100,
                              decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(5.0),
                                  image: new DecorationImage(
                                      image: new NetworkImage(
                                          'https://res.cloudinary.com/sfp/image/upload/c_fill,q_60,w_310,h_372/oth/FunimationStoreFront/1838817/English/1838817_English_ShowThumbnail_de02df2a-dacc-e811-8175-020165574d09.jpg'),
                                      fit: BoxFit.cover)),
                            ),
                            new Text("Tokyo ghoul",
                                style: new TextStyle(fontSize: 17.0),
                                textAlign: TextAlign.center),
                          ],
                        ),
                      ),
                    ),
                    new SizedBox(
                      width: 5.0,
                    ),
                    new Expanded(
                      child: Container(
                        height: 150.0,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 100,
                              decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(5.0),
                                  image: new DecorationImage(
                                      image: new NetworkImage(
                                          'https://i.pinimg.com/736x/58/9d/a6/589da61c3682d0dc2470dab8df3b6fc4--kawaii-girls-anime-girl-anime-art.jpg'),
                                      fit: BoxFit.cover)),
                            ),
                            new Text("Kuran",
                                style: new TextStyle(fontSize: 17.0),
                                textAlign: TextAlign.center),
                          ],
                        ),
                      ),
                    ),
                    new SizedBox(
                      width: 5.0,
                    ),
                    new Expanded(
                      child: Container(
                        height: 150.0,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 100,
                              decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(5.0),
                                  image: new DecorationImage(
                                      image: new NetworkImage(
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQUOsBqsTP5spcWgu_Xq4m240vGB_jNGlE-ZXsN64DMgb1ubKO9'),
                                      fit: BoxFit.cover)),
                            ),
                            new Text("fairy tail",
                                style: new TextStyle(fontSize: 17.0),
                                textAlign: TextAlign.center),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              
             ],
            ),
          ),
        )
      ],
    );
  }
}
