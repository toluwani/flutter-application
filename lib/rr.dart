import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
    int bottomNavIndex= 0;

  @override
  Widget build(BuildContext context) {
        return Scaffold(
          bottomNavigationBar: new BottomNavigationBar( 
            type: BottomNavigationBarType.fixed,
            fixedColor: Color(0xFF18D191),
            currentIndex:bottomNavIndex,
        onTap: (int index){
setState(() {
  bottomNavIndex =index; 
});
        },
        items: [
          new BottomNavigationBarItem(
            title:new Text(""),
          icon: new Icon(Icons.home)
          ),
          new BottomNavigationBarItem(
            title:new Text(""),
          icon: new Icon(Icons.local_offer)
          ),
           new BottomNavigationBarItem(
            title:new Text(""),
          icon: new Icon(Icons.message)
          ),
           new BottomNavigationBarItem(
            title:new Text(""),
          icon: new Icon(Icons.notification_important)
          ),
        ],
      ),
      appBar: new AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        iconTheme: new IconThemeData(color: Color(0xFF18D191)),
      ),
      body: MainContent(),
    );
  }
}

class MainContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: new Container(
            child: new Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    new Text(
                      "Explore",
                      style: new TextStyle(
                        fontSize: 20.0,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
                new SizedBox(
                  height: 12.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                        child: Padding(
                      padding: const EdgeInsets.only(right: 5.0),
                      child: new Container(
                        height: 100.0,
                        decoration: new BoxDecoration(
                            borderRadius: new BorderRadius.circular(5.0),
                            color: Color(0xFFFC6A7F)),
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Icon(
                              Icons.drive_eta,
                              color: Colors.white,
                            ),
                            new Text(
                              "Cars",
                              style: new TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                      ),
                    )),
                    new Expanded(
                        child: new Container(
                      height: 100.0,
                      child: Column(
                        children: <Widget>[
                          Expanded(
                              child: Padding(
                            padding:
                                const EdgeInsets.only(bottom: 2.5, right: 2.5),
                            child: new Container(
                              decoration: new BoxDecoration(
                                  color: Color(0xFF18D191),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: new Icon(Icons.local_offer,
                                        color: Colors.white),
                                  ),
                                  new Text("classified",
                                      style: new TextStyle(color: Colors.white))
                                ],
                              ),
                            ),
                          )),
                          Expanded(
                              child: Padding(
                            padding:
                                const EdgeInsets.only(top: 2.5, right: 2.5),
                            child: new Container(
                              decoration: new BoxDecoration(
                                  color: Color(0XFFFC7B4D),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: new Icon(Icons.beenhere,
                                        color: Colors.white),
                                  ),
                                  new Text("service",
                                      style: new TextStyle(color: Colors.white))
                                ],
                              ),
                            ),
                          )),
                        ],
                      ),
                    )),
                    new Expanded(
                        child: new Container(
                      height: 100.0,
                      child: Column(
                        children: <Widget>[
                          Expanded(
                              child: Padding(
                            padding:
                                const EdgeInsets.only(left: 2.5, bottom: 2.5),
                            child: new Container(
                              decoration: new BoxDecoration(
                                  color: Color(0XFF53CEDB),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: new Icon(Icons.account_balance,
                                        color: Colors.white),
                                  ),
                                  new Text("properties",
                                      style: new TextStyle(color: Colors.white))
                                ],
                              ),
                            ),
                          )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.only(left: 2.5, top: 2.5),
                            child: new Container(
                              decoration: new BoxDecoration(
                                  color: Color(0xFFFFCE56),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: new Icon(Icons.art_track,
                                        color: Colors.white),
                                  ),
                                  new Text("Jobs",
                                      style: new TextStyle(color: Colors.white))
                                ],
                              ),
                            ),
                          )),
                        ],
                      ),
                    )),
                  ],
                ),
                new SizedBox(
                  height: 15.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: new Text("popular now",
                          style: new TextStyle(fontSize: 18.0)),
                    ),
                    new Expanded(
                      child: new Text(
                        "view all ",
                        style: new TextStyle(
                            fontSize: 15.0, color: Color(0xFF18D191)),
                        textAlign: TextAlign.end,
                      ),
                    )
                  ],
                ),
                new SizedBox(
                  height: 12.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: Container(
                        height: 150.0,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 100,
                              decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(5.0),
                                  image: new DecorationImage(
                                      image: new NetworkImage(
                                          'https://www.howtogeek.com/wp-content/uploads/2016/01/steam-and-xbox-controllers.jpg'),
                                      fit: BoxFit.cover)),
                            ),
                            new Text("playstation",
                                style: new TextStyle(fontSize: 17.0),
                                textAlign: TextAlign.center),
                          ],
                        ),
                      ),
                    ),
                    new SizedBox(
                      width: 5.0,
                    ),
                    new Expanded(
                      child: Container(
                        height: 150.0,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 100,
                              decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(5.0),
                                  image: new DecorationImage(
                                      image: new NetworkImage(
                                          'https://pawanjewellers.in/wp-content/uploads/2016/09/Jewellery-new.jpg'),
                                      fit: BoxFit.cover)),
                            ),
                            new Text("Watches",
                                style: new TextStyle(fontSize: 17.0),
                                textAlign: TextAlign.center),
                          ],
                        ),
                      ),
                    ),
                    new SizedBox(
                      width: 5.0,
                    ),
                    new Expanded(
                      child: Container(
                        height: 150.0,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 100,
                              decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(5.0),
                                  image: new DecorationImage(
                                      image: new NetworkImage(
                                          'http://images4.fanpop.com/image/photos/21600000/Electronics-hd-wallpaper-21627626-1920-1200.jpg'),
                                      fit: BoxFit.cover)),
                            ),
                            new Text("Electronics",
                                style: new TextStyle(fontSize: 17.0),
                                textAlign: TextAlign.center),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              
             
              new SizedBox(
                  height: 15.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: new Text("popular now",
                          style: new TextStyle(fontSize: 18.0)),
                    ),
                    new Expanded(
                      child: new Text(
                        "view all ",
                        style: new TextStyle(
                            fontSize: 15.0, color: Color(0xFF18D191)),
                        textAlign: TextAlign.end,
                      ),
                    )
                  ],
                ),
                new SizedBox(
                  height: 12.0,
                ),
                Row(
                  children: <Widget>[
                    new Expanded(
                      child: Container(
                        height: 150.0,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 100,
                              decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(5.0),
                                  image: new DecorationImage(
                                      image: new NetworkImage(
                                          'https://s1.cdn.autoevolution.com/images/gallery/LEXUS-HS-250h-3892_26.jpg'),
                                      fit: BoxFit.cover)),
                            ),
                            new Text("cars",
                                style: new TextStyle(fontSize: 17.0),
                                textAlign: TextAlign.center),
                          ],
                        ),
                      ),
                    ),
                    new SizedBox(
                      width: 5.0,
                    ),
                    new Expanded(
                      child: Container(
                        height: 150.0,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 100,
                              decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(5.0),
                                  image: new DecorationImage(
                                      image: new NetworkImage(
                                          'https://d3tvpxjako9ywy.cloudfront.net/blog/content/uploads/2015/03/company-culture-why-it-matters.jpg?av=6219bb831e993c907ca622baef062556'),
                                      fit: BoxFit.cover)),
                            ),
                            new Text("people",
                                style: new TextStyle(fontSize: 17.0),
                                textAlign: TextAlign.center),
                          ],
                        ),
                      ),
                    ),
                    new SizedBox(
                      width: 5.0,
                    ),
                    new Expanded(
                      child: Container(
                        height: 150.0,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 100,
                              decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(5.0),
                                  image: new DecorationImage(
                                      image: new NetworkImage(
                    'http://images4.fanpop.com/image/photos/21600000/Electronics-hd-wallpaper-21627626-1920-1200.jpg'),
                                      fit: BoxFit.cover)),
                            ),
                            new Text("Electronics",
                                style: new TextStyle(fontSize: 17.0),
                                textAlign: TextAlign.center),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              
             ],
            ),
          ),
        )
      ],
    );
  }
}
